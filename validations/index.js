const Joi = require('joi').extend(require('@joi/date'));

const fieldsValidations = {
  checkin: Joi.date().required().format('YYYY-MM-DD').min('now').messages({
    'date.min': 'Check-in date should be greater than today'
  }),
  checkout: Joi.date().format('YYYY-MM-DD').greater(Joi.ref('checkin')).messages({
    'date.greater': 'Check-out date should be greater than check-in',
  })
};

function getFieldErrors(schema, data) {
  const { error, value: values } = schema.validate(data, {
    abortEarly: false
  });

  return {
    values: error ? values : {},
    errors: error
      ? error.details.map(({ message }) => message)
      : []
  };
}

function searchValidation(data) {
  const { checkin, checkout } = fieldsValidations;
  const schema = Joi.object({ checkin, checkout });

  return getFieldErrors(schema, data);
}

module.exports = { searchValidation };
