function nextDate(dayIndex) {
  const today = new Date();
  today.setDate(today.getDate() + (dayIndex - 1 - today.getDay() + 7) % 7 + 1);
  return today;
}

module.exports = { nextDate };
