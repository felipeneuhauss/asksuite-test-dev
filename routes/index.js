const express = require('express');
const index = express.Router();

index.get('/', (req, res) => {
    res.send('Hello Asksuite World!');
});

module.exports = index;
