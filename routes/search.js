const SearchHotel = require('./../services/HotelRoomSearchService');
const searchMiddleware = require('./../middlewares/search');
const HotelRoomSearchCrowlerStrategy = require('./../strategies/HotelRoomSearchCrowlerStrategy');

const express = require('express');
const index = express.Router();

index.post('/search', searchMiddleware, async (req, res) => {
    const params = req.body;
    const searchHotel = new SearchHotel();
    searchHotel.setStrategy(new HotelRoomSearchCrowlerStrategy());
    const result = await searchHotel.search(params);
    res.json(result);
});

module.exports = index;
