const { searchValidation } = require('./../validations');

const searchMiddleware = (req, res, next) => {
  const params = req.body;
  const { errors } = searchValidation(params);
  if (errors.length) {
    return res.status(422).json({
      errors
    });
  }
  next();
};

module.exports = searchMiddleware;
