require('dotenv').config();
const path = require('path');
const { ok } = require('assert');
const morgan = require('morgan');
const cors = require('cors');
const hpp = require('hpp');
const bodyParser = require('body-parser');
const env = process.env.NODE_ENV || 'development';
console.log('[env]', env);

ok((env !== 'development' || env !== 'production' || env !== 'test'), 'No env variable defined');

const configPath = path.join(__dirname, './config', `.env.${env}`);

require('dotenv').config({path: configPath});

const express = require('express');
const app = express();

if (env === 'development') {
    app.use(morgan(':method :url :response-time'));
}

app.use(cors());
app.use(hpp());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.text());
app.use(bodyParser.json({type: 'application/json'}));

const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');

const indexRouter = require('./routes/index.js');
const searchRouter = require('./routes/search.js');

app.use(indexRouter);
app.use(searchRouter);

app.use(
  '/api-docs',
  swaggerUi.serve,
  swaggerUi.setup(swaggerDocument)
);

const port = process.env.PORT;

app.listen(port || 8080, () => {
    console.log(`Listening on port ${port}`);
});

module.exports = app; // test purpose
