let chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../server');
const { format, addDays, subDays, addMonths } = require('date-fns');
const { nextDate } = require('./../utils');

const should = chai.should();
const expect = chai.expect

chai.use(chaiHttp);

// @see
const TIMEOUT = 100000;

describe('/search', function () {
  it('Should success search', function (done) {
    const checkIn = addMonths(new Date(), 1);
    const payload = {
      checkin: format(checkIn, 'yyyy-MM-dd'),
      checkout: format(addDays(checkIn, 2), 'yyyy-MM-dd')
    };
    chai.request(server).post('/search').send(payload).end((err, res) => {
      console.log('res.body[0]', res.body[0])
      res.should.have.status(200);
      res.body.should.be.a('array');
      expect(res.body[0]).to.have.keys(['name', 'description', 'price', 'image'])
      done();
    });
  }).timeout(TIMEOUT);

  it('Should empty data when checkout is saturday', function (done) {
    const checkIn = nextDate(4);
    const checkOut = nextDate(6);
    const payload = {
      checkin: format(checkIn, 'yyyy-MM-dd'),
      checkout: format(checkOut, 'yyyy-MM-dd')
    };

    chai.request(server).post('/search').send(payload).end((err, res) => {
      console.log('res', res.body)
      res.should.have.status(200);
      res.body.should.be.a('array').equal([]);
      done();
    });
  }).timeout(TIMEOUT);

  it('Should fail when check in is lower or equal today', function (done) {
    const checkIn = addDays(new Date(), 1);
    const payload = {
      checkin: format(subDays(checkIn, 2), 'yyyy-MM-dd'),
      checkout: format(addDays(checkIn, 2), 'yyyy-MM-dd')
    };
    chai.request(server).post('/search').send(payload).end((err, res) => {
      res.should.have.status(422);
      res.body.should.be.an('object');
      res.body.should.have.property('errors').eql(['Check-in date should be greater than today'])
      done();
    });
  });

  it('Should fail when check out is lower than check-in', function (done) {
    const checkIn = addDays(new Date(), 1);
    const payload = {
      checkin: format(checkIn, 'yyyy-MM-dd'),
      checkout: format(subDays(checkIn, 2), 'yyyy-MM-dd')
    };
    chai.request(server).post('/search').send(payload).end((err, res) => {
      res.should.have.status(422);
      res.body.should.be.an('object');
      res.body.should.have.property('errors').eql(['Check-out date should be greater than check-in'])
      done();
    });
  });
});
