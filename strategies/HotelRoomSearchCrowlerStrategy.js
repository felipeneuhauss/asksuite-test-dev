const BrowserService = require('./../services/BrowserService');
const { format, parseISO } = require('date-fns');

class HotelRoomSearchCrowlerStrategy {
  async getPage (url) {
    if (url) {
      return new Error('A searchable URL should be defined');
    }
    this.browser = await BrowserService.getBrowser();
    return await this.browser.newPage();
  }

  getUrl({checkin, checkout}) {
    checkin = format(parseISO(checkin), 'ddMMyyyy');
    checkout = format(parseISO(checkout), 'ddMMyyyy');
    return `https://book.omnibees.com/hotelresults?CheckIn=${checkin}&CheckOut=${checkout}&` +
      `Code=AMIGODODANIEL&NRooms=1&_askSI=d34b1c89-78d2-45f3-81ac-4af2c3edb220&ad=2&ag=-&c=2983&ch=0&`+
      `diff=false&group_code=&lang=pt-BR&loyality_card=&utm_source=asksuite&q=5462#show-more-hotel-button`;
  }

  async finished() {
    await BrowserService.closeBrowser(this.browser);
  }

  async search(params) {
    const page = await this.getPage();
    await page.goto(this.getUrl(params));
    await page.waitForSelector('#hotels_grid');

    const roomList = await page.$$eval('.roomrate:not(.d-none)', roomItems => {
      roomItems = roomItems.filter(roomEl => {
        return !roomEl.querySelector('.los_restricted') ||
          roomEl.querySelector('.los_restricted').textContent.trim().indexOf('Dia fechado para saída') === -1;
      });

      if (!roomItems.length) {
        return [];
      }

      return roomItems.map((el) => {
        return {
          name: el.querySelector('.hotel_name').textContent.trim(),
          description: el.querySelector('.description').textContent.trim().replace(/\(ver mais\)/i, ''),
          image: el.querySelector('.image-step2').src,
          price: el.querySelector('.price-total').textContent.trim()
        };
      });
    });
    await this.finished();
    return roomList || [];
  }
}

module.exports = HotelRoomSearchCrowlerStrategy;
