class HotelRoomSearchService {

  setStrategy(strategy) {
    this.strategy = strategy;
  }

  async search(params) {
    try {
      return this.strategy.search(params);
    } catch (e) {
      console.error(e);
    }
  }
}

module.exports = HotelRoomSearchService;
